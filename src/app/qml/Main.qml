import QtQuick
import QtQuick.Window
import QtQuick.Layouts
import QtQuick.Controls

Window {
    width: 800
    height: 480
    visible: true

    Item {
        id: viewMain
        anchors.fill: parent
        visible: true

        ColumnLayout {
            anchors.fill: parent

            Rectangle {
                id: buttonIrrigation
                focus: true
                Image {
                    source: "qrc:/resources/img/irrigation.png"
                }
                Layout.fillWidth: true
                Layout.fillHeight: true
                height: 200
                Text {
                    id: textIrrigation
                    font.pixelSize: 100
                    color: "#CCFFFF"
                    smooth: true
                    width: parent.width
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Irrigation")
                }
                Rectangle {
                    id: selectIrrigation
                    width: parent.width
                    height: parent.height
                    border.color: "#FF0000"
                    border.width: 10
                    color: "transparent"
                    visible: true
                }
                Keys.onDownPressed: () => {
                                        selectIrrigation.visible = false
                                        selectLighting.visible = true
                                        buttonLighting.focus = true
                                    }
                Keys.onReturnPressed: () => {
                                        viewMain.visible = false
                                     }
            }
            Rectangle {
                id: buttonLighting
                Image {
                    source: "qrc:/resources/img/lighting.png"
                }
                Layout.fillWidth: true
                Layout.fillHeight: true
                height: 200
                Text {
                    id: textLighting
                    font.pixelSize: 100
                    color: "#FFCCCC"
                    smooth: true
                    width: parent.width
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Lighting")
                }
                Rectangle {
                    id: selectLighting
                    width: parent.width
                    height: parent.height
                    border.color: "#FF0000"
                    border.width: 10
                    color: "transparent"
                    visible: false
                }
                Keys.onUpPressed: () => {
                                        selectLighting.visible = false
                                        selectIrrigation.visible = true
                                        buttonIrrigation.focus = true
                                    }
                Keys.onReturnPressed: () => {
                                        viewMain.visible = false
                                     }
            }
        }
    }
}
